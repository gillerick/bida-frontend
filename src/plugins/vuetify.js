import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

const vuetify = new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#EA7125',
                secondary: '#C4C4C4',
                success: '#22CB47',
                anchor: '#8c9eff',
            },
        },
    },
})

export default vuetify
