import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import VueGeolocation from "vue-browser-geolocation/src";


Vue.config.productionTip = false
Vue.use(VueGeolocation)

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load:{
    key: 'AIzaSyAS-soEXp_eCcyQ9JrT-qDOBapj-l5m2to',
    libraries: 'places',
  }
})

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
