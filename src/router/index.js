import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Signup from "@/views/Signup";
import SignIn from "@/views/Signin";
import CustomerInformation from "@/views/Details";
import Map from "@/views/EditDetails";
import Wallets from "@/views/Wallets";
import Wallet from "@/views/Wallet";
import PaymentFailed from "@/views/PaymentFailed";
import Products from "@/views/Products";
import Notifications from "@/views/Notifications";
import Orders from "@/views/Orders";
import SavedProducts from "@/views/SavedProducts";
import SavedLocations from "@/views/SavedLocations";
import Product from "@/views/Product";
import OrderHistory from "@/views/OrderHistory";
import OrderDetails from "@/views/OrderDetails";
import Cart from "@/views/Cart";
import WalletEmpty from "@/views/WalletEmpty";
import PaymentSucceeded from "@/views/PaymentSucceeded";
import Dashboard from "@/views/Dashboard";
import TrackOrder from "@/views/TrackOrder";
import Checkout from "@/views/Checkout";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/signup',
    name: 'sign_up',
    component: Signup
  },
  {
    path: '/login',
    name: 'sign_in',
    component: SignIn
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard
  },
  {
    path: '/customer/info',
    name: 'customer_information',
    component: CustomerInformation
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: Checkout
  },
  {
    path: '/map',
    name: 'map',
    component: Map
  },
  {
    path: '/wallets',
    name: 'wallets',
    component: Wallets
  },
  {
    path: '/wallet',
    name: 'wallet',
    component: Wallet
  },
  {
    path: '/payment-fail',
    name: 'payment_fail',
    component: PaymentFailed
  },
  {
    path: '/payment-success',
    name: 'payment_success',
    component: PaymentSucceeded
  },
  {
    path: '/products',
    name: 'products',
    component: Products
  },
  {
    path: '/notifications',
    name: 'notifications',
    component: Notifications
  },
  {
    path: '/orders',
    name: 'orders',
    component: Orders
  },
  {
    path: '/favourites',
    name: 'saved_products',
    component: SavedProducts
  },
  {
    path: '/locations',
    name: 'saved_locations',
    component: SavedLocations
  },
  {
    path: '/product',
    name: 'product',
    component: Product
  },
  {
    path: '/order-history',
    name: 'history',
    component: OrderHistory
  },
  {
    path: '/order-details',
    name: 'order_details',
    component: OrderDetails
  },
  {
    path: '/cart',
    name: 'cart',
    component: Cart
  },
  {
    path: '/wallet-empty',
    name: 'empty_wallet',
    component: WalletEmpty
  },
  {
    path: '/order/track',
    name: 'track-order',
    component: TrackOrder
  },

]

const router = new VueRouter({
  routes
})

export default router
